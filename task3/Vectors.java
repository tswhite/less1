package task3;

public class Vectors {

    private double[] coord = new double[3];

    public Vectors(double corA, double corB, double corC) {
        this.coord[0]=corA;
        this.coord[1]=corB;
        this.coord[2]=corC;
    }

    public Vectors add(Vectors vector){
        double[] a;
        double[] b;
        a = this.coord;
        b=vector.coord;
        return new Vectors (a[0]+b[0], a[1]+b[1], a[2]+b[2]);
    }

    public double scalarMultiply(Vectors vector){
        double[] a;
        double[] b;
        a = this.coord;
        b=vector.coord;
        return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
    }
}



