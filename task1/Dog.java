package task1;

public class Dog {

    private String breed;
    private String color;
    private String size;
    private int weight;
    private double speed;

    Dog(String breed, String color, String size, int weight) {
        this.breed = breed;
        this.color = color;
        this.size = size;
        this.weight = weight;
    }

    public void gavSound(){
        System.out.println("\"Gav\" - пес гавкає");
    }

    public double run(double delta){
        System.out.println("Пес почав рухатись зі швидкістю "+ delta +" m/s" );
        return this.speed = delta;
    }

    public double increase(double delta){
        System.out.println("Пес пришвидшився на "+ delta +" m/s");
        return this.speed= this.speed + delta;
    }

    public double stop(){
        System.out.println("Пес зупинився");;
        return this.speed = 0;
    }

    public void checkWeight(){
        System.out.println("Вага пса = "+weight+" кг");
    }

    public void checkSpeed(){
        System.out.println("поточна швидкість = "+ this.speed);
    }

}
