package task1;

public class Main {
    public static void main(String[] args) {
        Dog dog1 = new Dog("spitz","brown","small",5);
        Dog dog2 = new Dog("shar pei","grey","medium",23);

        dog1.checkWeight();
        dog1.gavSound();
        dog1.run(2);
        dog1.increase(0.5);
        dog1.checkSpeed();
        dog1.increase(0.7);
        dog1.checkSpeed();
        dog1.stop();

        dog2.checkWeight();
        dog2.gavSound();
        dog2.run(2);
        dog2.checkSpeed();
        dog2.increase(1.7);
        dog2.checkSpeed();
        dog2.stop();


    }
}
