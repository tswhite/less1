package task2;

public class Triangle {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public double calcSquare() {
        if (this.sideA + this.sideB <= this.sideC ||this.sideA + this.sideC <= this.sideB ||this.sideB + this.sideC <= this.sideA) {
            System.out.println( "У трикутника сума будь-яких двох сторін повинна бути більше третьою. Інакше дві сторони просто \"ляжуть\" на третю і трикутника не вийде.");;
        return 0;
        } else {
            double p = (this.sideA + this.sideB + this.sideC) / 2;
            double inter = p * (p - this.sideA) * (p - this.sideB) * (p - this.sideC);
            double s = Math.sqrt(inter);
            System.out.println("Площа трикутника дорівнює  " + s);
            return s;
        }

    }


}
